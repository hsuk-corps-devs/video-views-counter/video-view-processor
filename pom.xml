<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.hsuk</groupId>
	<artifactId>video-views-processor</artifactId>
	<version>1.0.5-SNAPSHOT</version>
	<name>video-views-processor</name>
	<description>Video views kafka consumer and processor</description>
	<properties>
		<java.version>1.8</java.version>
		<maven.compiler.target>1.8</maven.compiler.target>
		<maven.compiler.source>1.8</maven.compiler.source>
		<docker.repo.url>192.168.1.130:5000</docker.repo.url>
	</properties>

	<scm>
		<connection>scm:git:https://gitlab.com/video-views-counter/video-view-processor.git</connection>
		<developerConnection>scm:git:https://gitlab.com/video-views-counter/video-view-processor.git</developerConnection>
		<tag>HEAD</tag>
	</scm>

	<dependencies>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>1.18.12</version>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-core_2.11</artifactId>
			<version>2.4.1</version>
			<scope>${artifact.scope}</scope>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-log4j12</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-sql_2.11</artifactId>
			<version>2.4.1</version>
			<scope>${artifact.scope}</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-streaming_2.11</artifactId>
			<version>2.4.1</version>
			<scope>${artifact.scope}</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.spark</groupId>
			<artifactId>spark-streaming-kafka-0-10_2.11</artifactId>
			<version>2.4.5</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.apache.kafka/kafka-clients -->
		<dependency>
			<groupId>org.apache.kafka</groupId>
			<artifactId>kafka-clients</artifactId>
			<version>2.4.1</version>
			<scope>${artifact.scope}</scope>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.scala-lang.modules/scala-xml -->
		<dependency>
			<groupId>org.scala-lang.modules</groupId>
			<artifactId>scala-xml_2.11</artifactId>
			<version>1.2.0</version>
			<scope>${artifact.scope}</scope>
		</dependency>

		<dependency>
			<groupId>com.rabbitmq</groupId>
			<artifactId>amqp-client</artifactId>
			<version>3.0.4</version>
		</dependency>

		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>2.6</version>
			<scope>${artifact.scope}</scope>
        </dependency>

        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
            <version>2.10.3</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-core</artifactId>
            <version>1.2.3</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.2.3</version>
		</dependency>
		<dependency>
			<groupId>redis.clients</groupId>
			<artifactId>jedis</artifactId>
			<version>2.8.1</version>
		</dependency>
	</dependencies>

	<repositories>

		<repository>
			<id>central</id>
			<url>http://nexus.corp.hsuk.com/content/repositories/central/</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>

		<repository>
			<id>snapshots</id>
			<url>http://nexus.corp.hsuk.com/content/repositories/snapshots/</url>
		</repository>

		<repository>
			<id>releases</id>
			<url>http://nexus.corp.hsuk.com/content/repositories/releases/</url>
		</repository>

	</repositories>

	<pluginRepositories>
		<pluginRepository>
			<id>central</id>
			<url>http://nexus.corp.hsuk.com/content/repositories/central/</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</pluginRepository>
	</pluginRepositories>

	<distributionManagement>
		<snapshotRepository>
			<id>snapshots</id>
			<url>http://nexus.corp.hsuk.com/content/repositories/snapshots/</url>
			<uniqueVersion>false</uniqueVersion>
		</snapshotRepository>

		<repository>
			<id>releases</id>
			<url>http://nexus.corp.hsuk.com/content/repositories/releases/</url>
		</repository>
	</distributionManagement>

	<build>
		<plugins>
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<archive>
						<manifest>
							<mainClass>com.hsuk.video.views.processor.ViewsProcessorAppMain</mainClass>
						</manifest>
					</archive>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
				</configuration>
				<executions>
					<execution>
						<id>make-assembly</id> <!-- this is used for inheritance merges -->
						<phase>package</phase> <!-- bind to the packaging phase -->
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<configuration>
					<archive>
						<manifest>
							<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
							<addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
						</manifest>
					</archive>
				</configuration>
			</plugin>

			<plugin>
				<groupId>com.spotify</groupId>
				<artifactId>dockerfile-maven-plugin</artifactId>
				<version>1.4.13</version>
				<configuration>
					<useMavenSettingsForAuth>true</useMavenSettingsForAuth>
					<repository>${docker.repo.url}/video-views-processor</repository>
					<tag>${project.version}</tag>
					<buildArgs>
						<JAR_FILE>target/${project.build.finalName}-jar-with-dependencies.jar</JAR_FILE>
						<APP_ENV>${app.env}</APP_ENV>
					</buildArgs>
				</configuration>
				<executions>
					<execution>
						<id>default</id>
						<phase>install</phase>
						<goals>
							<goal>build</goal>
							<goal>push</goal>
						</goals>
					</execution>
				</executions>

			</plugin>

		</plugins>

	</build>

	<profiles>
		<profile>
			<id>qaa</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>

			<properties>
				<app.env>qaa</app.env>
				<artifact.scope>compile</artifact.scope>
			</properties>

		</profile>

		<profile>
			<id>prod</id>
			<activation>
				<activeByDefault>false</activeByDefault>
			</activation>

			<properties>
				<app.env>prod</app.env>
				<artifact.scope>compile</artifact.scope>
			</properties>

		</profile>

	</profiles>

</project>
