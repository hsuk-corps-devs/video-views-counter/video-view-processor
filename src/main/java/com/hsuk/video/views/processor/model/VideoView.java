package com.hsuk.video.views.processor.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class VideoView implements Serializable {
    private long videoId;
    private long timestamp;
    private long userId;
    private long sessionId;

    public String getJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        //Converting the Object to JSONString
        return mapper.writeValueAsString(this);
    }
}