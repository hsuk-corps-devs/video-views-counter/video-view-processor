package com.hsuk.video.views.processor.rabbitmq;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.SerializationUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * The endpoint that consumes messages off of the queue. Happens to be runnable.
 *
 * @author Kush
 */
@Slf4j
public class QueueConsumer extends EndPoint implements Runnable, Consumer {

    public QueueConsumer(String endPointName) throws IOException {
        super(endPointName);
    }

    public void run() {
        try {
            //start consuming messages. Auto acknowledge messages.
            channel.basicConsume(endPointName, true, this);
        } catch (IOException e) {
            log.error("Error consuming message from rabbiqMq:{}", e.getMessage());
        }
    }

    /**
     * Called when consumer is registered.
     */
    public void handleConsumeOk(String consumerTag) {
        log.info("Consumer:{} registered,", consumerTag);
    }

    /**
     * Called when new message is available.
     */
    public void handleDelivery(String consumerTag, Envelope env,
                               BasicProperties props, byte[] body) {
        Map map = (HashMap) SerializationUtils.deserialize(body);
        log.info("Message Number:{}  received.", map.get("message number"));
    }

    public void handleCancel(String consumerTag) {
        // Do nothing
    }

    public void handleCancelOk(String consumerTag) {
        // Do nothing
    }

    public void handleRecoverOk(String consumerTag) {
        // Do nothing
    }

    public void handleShutdownSignal(String consumerTag, ShutdownSignalException arg1) {
        // Do nothing
    }
}
