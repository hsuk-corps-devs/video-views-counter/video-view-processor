package com.hsuk.video.views.processor.constants;

public class AppConstants {

    //Kafka properties
    public static final String KAFKA_BOOTSTRAP_SERVERS_CONFIG = "kafka.bootstrap-servers";
    public static final String KAFKA_GROUP_ID_CONFIG = "kafka.groupId";
    public static final String KAFKA_REQUEST_TIMEOUT_MS_CONFIG = "kafka.timeout";
    public static final String KAFKA_AUTO_OFFSET_RESET_CONFIG = "kafka.auto.offset.reset.config";
    public static final String KAFKA_AUTO_COMMIT_INTERVAL_MS_CONFIG = "kafka.auto.commit.interval.config";
    public static final String KAFKA_VIDEOVIEW_TOPICS = "kafka.videoview.topics";
    public static final String KAFKA_STREAM_INTERVAL_IN_SECONDS = "kafka.stream.interval.in.seconds";

    //RabbitMQ properties
    public static final String RABBITMQ_HOST = "rabbitmq.host";
    public static final String RABBITMQ_QUEUENAME = "rabbitmq.queuename";

}