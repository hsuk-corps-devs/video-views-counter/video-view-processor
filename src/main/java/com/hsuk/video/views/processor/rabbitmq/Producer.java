package com.hsuk.video.views.processor.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * The producer endpoint that writes to the queue.
 *
 * @author Kush
 */
public class Producer extends EndPoint {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public Producer(String endPointName) throws IOException {
        super(endPointName);
    }

    public void sendMessage(byte[] object) {
        try {
            channel.basicPublish("", endPointName, null, object);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
