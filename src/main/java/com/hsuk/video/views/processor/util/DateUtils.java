package com.hsuk.videoviewerconsumer.util;

import java.io.Serializable;
import java.util.Calendar;

public class DateUtils implements Serializable {

    private static final int MINUTE_WINDOW = 5;

    public String getSuffix(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        int minute = calendar.get(Calendar.MINUTE);
        int grp = minute / MINUTE_WINDOW;
        calendar.set(Calendar.MINUTE, grp * MINUTE_WINDOW);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        long startTimestamp = calendar.getTimeInMillis();
        calendar.set(Calendar.MINUTE, (grp + 1) * MINUTE_WINDOW);
        long endTimestamp = calendar.getTimeInMillis();
        return startTimestamp + "_" + endTimestamp;
    }
}
