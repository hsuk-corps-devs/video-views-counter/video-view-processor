package com.hsuk.video.views.processor.cache;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Slf4j
public final class ResourceManager {
    private static final String REDIS_MASTER_HOST = "redis.master.host";
    private static final String REDIS_MASTER_PORT = "redis.master.port";
    private static final String REDIS_PASSWORD = "redis.server.password";
    private static final int TIMEOUT = 1212;

    private static final int MAX = 10;
    private JedisPool pool;

    public ResourceManager() {
        init();
    }

    private void init() {
        try (InputStream input = ResourceManager.class.getClassLoader().getResourceAsStream("redis.properties")) {
            Properties props = new Properties();
            props.load(input);
            int port = Integer.parseInt(props.getProperty(REDIS_MASTER_PORT));
            String masterhost = props.getProperty(REDIS_MASTER_HOST);
            pool = new JedisPool(getPoolConfig(), masterhost, port, TIMEOUT, props.getProperty(REDIS_PASSWORD));
            log.info("kafka service setup successful");
            log.info("this is property " + props.toString());
        } catch (IOException e) {
            log.error("Error reading property file", e);
        }
    }

    private JedisPoolConfig getPoolConfig() {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setTestOnBorrow(true);
        config.setMaxIdle(5);
        config.setMaxTotal(MAX);
        return config;
    }


    public Jedis getJedisConnection() {
        return pool.getResource();
    }

}
