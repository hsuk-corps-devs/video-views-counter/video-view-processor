package com.hsuk.video.views.processor.kafka.deserializers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hsuk.video.views.processor.model.VideoView;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Map;

public class VideoViewDeserializer implements Deserializer<VideoView> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void configure(Map map, boolean b) {

    }

    @Override
    public VideoView deserialize(String s, byte[] bytes) {
        ObjectMapper mapper = new ObjectMapper();
        VideoView obj = null;
        try {
            obj = mapper.readValue(bytes, VideoView.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return obj;
    }

    @Override
    public void close() {

    }
}