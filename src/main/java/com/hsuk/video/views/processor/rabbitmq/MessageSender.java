package com.hsuk.video.views.processor.rabbitmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hsuk.video.views.processor.cache.PropertiesCache;
import com.hsuk.video.views.processor.constants.AppConstants;
import com.hsuk.video.views.processor.model.VideoCount;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import scala.Tuple2;

import java.io.IOException;

@Slf4j
public class MessageSender {

    private static final char SEPARATOR = '_';
    private ObjectMapper objMapper = new ObjectMapper();

    public void publishToRabbitMQ(JavaPairDStream<String, Integer> videoViewCount) throws IOException {
        Producer rabbitMQProducer = new Producer(PropertiesCache.INSTANCE.getProperty(AppConstants.RABBITMQ_QUEUENAME));
        videoViewCount.foreachRDD(
                (VoidFunction<JavaPairRDD<String, Integer>>)
                        rdd -> {
                            for (Tuple2<String, Integer> tempVideoCount : rdd.collect()) {
                                String key = tempVideoCount._1;
                                int index = key.indexOf(SEPARATOR);
                                int secondIndex = key.lastIndexOf(SEPARATOR);
                                if (index < 0 || secondIndex < 0 || index == secondIndex) { // there is no timestamp provided
                                    log.error("No Timestamp information specified in the key " + key + ". Dropping the message");
                                } else {
                                    long videoId = Long.parseLong(key.substring(0, index));
                                    long startTimestamp = Long.parseLong(key.substring(index + 1, secondIndex));
                                    long endTimestamp = Long.parseLong(key.substring(secondIndex + 1));
                                    VideoCount videoCount = new VideoCount(
                                            videoId,
                                            tempVideoCount._2,
                                            PropertiesCache.getInstance().getProperty(AppConstants.KAFKA_GROUP_ID_CONFIG),
                                            startTimestamp,
                                            endTimestamp
                                    );
                                    log.info("Sending message: {}", videoCount);
                                    rabbitMQProducer.sendMessage(objMapper.writeValueAsBytes(videoCount));
                                }
                            }
                        }
        );
    }
}
