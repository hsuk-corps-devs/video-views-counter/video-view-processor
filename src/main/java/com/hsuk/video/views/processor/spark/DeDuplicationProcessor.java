package com.hsuk.video.views.processor.spark;

import com.hsuk.video.views.processor.cache.ResourceManager;
import com.hsuk.video.views.processor.model.VideoView;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.api.java.JavaDStream;
import redis.clients.jedis.Jedis;

@Slf4j
public class DeDuplicationProcessor {

    private static final int TIMEOUT = 5 * 60;
    private static final ResourceManager resourceManager = new ResourceManager();

    public JavaDStream<VideoView> process(JavaDStream<VideoView> messages) {
        return messages
                .filter((Function<VideoView, Boolean>)
                        videoView -> {
                            Jedis jedis = resourceManager.getJedisConnection();
                            String key = videoView.getVideoId() + "_" + videoView.getSessionId() + "_" + videoView.getTimestamp();
                            String viewJson = videoView.toString();
                            long rsps = jedis.setnx(key, viewJson);
                            boolean result;
                            if (rsps <= 0 && jedis.exists(key)) {
                                log.warn("kafka dup: {}", viewJson); // and other logic
                                result = false;
                            } else {
                                log.info("adding value: {}", viewJson);
                                // 2 hours is ok for production environment;
                                jedis.expire(key, TIMEOUT);
                                result = true;
                            }
                            jedis.close();
                            return result;
                        });
    }

}
//{"videoId":1,"timestamp":23,"userId":12,"sessionId":23}
//{"videoId":2,"timestamp":23,"userId":12,"sessionId":23}

