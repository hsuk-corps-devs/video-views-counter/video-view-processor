package com.hsuk.video.views.processor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class VideoCount {
    private Long videoId;
    private Integer count;
    private String groupId;
    private Long startTimestamp;
    private Long endTimestamp;
}
